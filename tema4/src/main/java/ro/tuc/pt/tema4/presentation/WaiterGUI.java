package ro.tuc.pt.tema4.presentation;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.IOException;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import ro.tuc.pt.tema4.business.BaseProduct;
import ro.tuc.pt.tema4.business.CompositeProduct;
import ro.tuc.pt.tema4.business.MenuItem;
import ro.tuc.pt.tema4.business.Order;
import ro.tuc.pt.tema4.business.Restaurant;
import ro.tuc.pt.tema4.business.RestaurantProcessing;
import ro.tuc.pt.tema4.data.BillGenerator;
import ro.tuc.pt.tema4.data.RestaurantSerializator;

/**
 * @author Vali
 *
 */
public class WaiterGUI extends JFrame implements RestaurantProcessing {
	private static final long serialVersionUID = -1064520642213754657L;
	
	private Restaurant restaurant;
	
	private Order waiterOrder;
	
	private int tableIndex;
	
	private JPanel panel;
	
	public WaiterGUI(String appTitle, Restaurant rest, int tableIndex) {
		this.setTitle(appTitle + " - Waiter window");
		this.setSize(980, 560);
		this.setVisible(true);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		this.restaurant = rest;
		
		this.waiterOrder = new Order(tableIndex);
		
		this.tableIndex = tableIndex;
		
		this.setLayout(new GridBagLayout());
		
		this.addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
		    	RestaurantSerializator.saveProgress(restaurant);
			}
		});
		
		loadMainPanel();
	}
	
	public WaiterGUI(String appTitle, Restaurant rest, int tableIndex, boolean isSerialized) {
		this(appTitle, rest, tableIndex);
		
		if (isSerialized) {
			Order foundOrder = getTableOrder();
			if (foundOrder != null && foundOrder != waiterOrder) {
				waiterOrder = foundOrder;
			}
		}
	}
	
	private void loadMainPanel() {
		if (panel != null)
			this.remove(panel);
		
		panel = new JPanel(new FlowLayout());
		
		JButton btnNewOrder = new JButton("New order");
		btnNewOrder.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				loadRegisterOrderPanel();
			}			
		});
		panel.add(btnNewOrder);
		
		JButton btnPrice = new JButton("Total price");
		btnPrice.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				double price = (waiterOrder == null) ? 0.0 : waiterOrder.computePrice();
				JOptionPane.showMessageDialog(null, 
						String.format("Total price until now: %.2f", price), 
						"Total", JOptionPane.INFORMATION_MESSAGE);
			}			
		});
		panel.add(btnPrice);
		
		JButton btnGenerateBill = new JButton("Generate bill");
		btnGenerateBill.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {				
				if (waiterOrder != null) {
					generateBill(waiterOrder);
				} else {
					JOptionPane.showMessageDialog(null, 
							"Could not find any registered order", "Error", JOptionPane.ERROR_MESSAGE);
				}
			}			
		});
		panel.add(btnGenerateBill);
		
		this.add(panel);
		
		this.revalidate();
		this.repaint();
	}

	private void loadRegisterOrderPanel() {
		if (restaurant.getMenuList() == null) {
			JOptionPane.showMessageDialog(this, "Could not find any menu items in menu.", "Error", JOptionPane.ERROR_MESSAGE);
			return;
		}
		
		if (panel != null)
			this.remove(panel);
		
		panel = new JPanel(new BorderLayout());
		
		JPanel centerPanel = new JPanel(new GridLayout(1, 2));
		
		final DefaultTableModel menuTableModel = new DefaultTableModel() {
			private static final long serialVersionUID = 4978760362290085314L;
			
			@Override
		    public boolean isCellEditable(int row, int column) {
		       return false;
		    }
		};
		
		final DefaultTableModel orderTableModel = new DefaultTableModel() {
			private static final long serialVersionUID = 4978760362290085314L;

			@Override
		    public boolean isCellEditable(int row, int column) {
		       return false;
		    }
		};
		
		{
			final JTable menuTable = new JTable();
			
			menuTableModel.setColumnIdentifiers(new String[] { "ID", "Name", "Contains", "Price" });
			int count = 0;
			for (MenuItem mi : restaurant.getMenuList()) {
				if (mi instanceof BaseProduct) {
					BaseProduct p = (BaseProduct) mi;
					
					Object[] row = new Object[4];
					row[0] = count++;
					row[1] = p.getName();
					row[2] = "-";
					row[3] = p.computePrice();
					menuTableModel.addRow(row);
				} else if (mi instanceof CompositeProduct) {
					CompositeProduct p = (CompositeProduct) mi;
					
					Object[] row = new Object[4];
					row[0] = count++;
					row[1] = p.getName();
					row[2] = p.getItemsNames();
					row[3] = p.computePrice();
					menuTableModel.addRow(row);
				}
			}
			menuTable.setModel(menuTableModel);
			
			menuTable.addMouseListener(new MouseAdapter() {
				public void mouseClicked(MouseEvent arg0) {
					if (arg0.getClickCount() == 2) {
						int rowSelected = menuTable.getSelectedRow();
						Object[] row = new Object[4];
						for (int i = 0; i < 4; ++i)
							row[i] = menuTable.getValueAt(rowSelected, i);
						
						orderTableModel.addRow(row);
					}
				}
			});
			
			JScrollPane spTable = new JScrollPane(menuTable, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
			centerPanel.add(spTable);
		}
		
		{
			final JTable table = new JTable();
		
			orderTableModel.setColumnIdentifiers(new String[] { "ID", "Name", "Contains", "Price" });
			
			table.setModel(orderTableModel);
			
			table.addMouseListener(new MouseAdapter() {
				public void mouseClicked(MouseEvent arg0) {
					if (arg0.getClickCount() == 2) {
						int rowSelected = table.getSelectedRow();
						orderTableModel.removeRow(rowSelected);
					}
				}
			});
			
			JScrollPane spTable = new JScrollPane(table, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
			centerPanel.add(spTable);
		}
		panel.add(centerPanel, BorderLayout.CENTER);
		
		JButton btnSend = new JButton("Submit Order");
		btnSend.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				int rowCount = orderTableModel.getRowCount();
				for (int i = 0; i < rowCount; ++i) {
					String contains = (String) orderTableModel.getValueAt(i, 2);
					String name = (String) orderTableModel.getValueAt(i, 1);
					if (!contains.equals("-")) {
						// CompositeProduct
						CompositeProduct p = null;
						for (MenuItem mi : restaurant.getMenuList()) {
							if (mi instanceof CompositeProduct && ((CompositeProduct) mi).getName().equals(name)) {
								p = (CompositeProduct) mi;
							}
						}
						waiterOrder.add(p);
					} else {
						// BaseProduct
						BaseProduct p = null;
						for (MenuItem mi : restaurant.getMenuList()) {
							if (mi instanceof BaseProduct && ((BaseProduct) mi).getName().equals(name)) {
								p = (BaseProduct) mi;
							}
						}
						waiterOrder.add(p);
					}
				}
				
				createNewOrder(waiterOrder);
				
				JOptionPane.showMessageDialog(null, "Order submitted.", "Success", JOptionPane.INFORMATION_MESSAGE);
				loadMainPanel();
			}
		});
		panel.add(btnSend, BorderLayout.PAGE_END);
		
		this.add(panel);
		this.revalidate();
		this.repaint();
	}

	private Order getTableOrder() {
		for (Order o : restaurant.getOrderList()) 
			if (o.getTableId() == this.tableIndex)
				return o;
		return null;
	}
	
	public void createNewMenuItem(MenuItem item) { }

	public void deleteMenuItem(MenuItem item) { }

	public void editMenuItem(MenuItem old_item, MenuItem new_item) { }

	public void createNewOrder(Order ord) {
		boolean tableExists = false;
		Order t = null;
		for (Order o : restaurant.getOrderList()) {
			if (o.getTableId() == ord.getTableId()) {
				tableExists = true;
				t = o;
				break;
			}
		}
		
		if (tableExists) {
			for (MenuItem mi : ord.getMenuItems()) {
				if (!t.getMenuItems().contains(mi)) {
					t.add(mi);
				}
			}
		} else {
			restaurant.getOrderList().add(ord);
		}
		
		restaurant.updateObservers();
	}

	public double computePrice(Order o) {
		return o.computePrice();
	}

	public void generateBill(Order o) {
		String str = new String();
		str += restaurant.getName() + "\n";
		str += "\n";
		double total = 0.0;
		for (MenuItem mi : o.getMenuItems()) {
			str += mi.toString() + "\n";
			total += mi.computePrice();
		}
		str += "\n";
		str += "Total: " + String.format("%.2f", total) + "\n";
		
		try {
			String billName = BillGenerator.createNewBill(str);
			JOptionPane.showMessageDialog(this, "Bill generated, file name: " + billName, "Success", JOptionPane.INFORMATION_MESSAGE);
			restaurant.getOrderList().remove(o);
			restaurant.updateObservers();
		} catch (IOException e) {
			JOptionPane.showMessageDialog(this, 
					"Could not generate bill.", 
					"Error", 
					JOptionPane.ERROR_MESSAGE);
		}
	}
}
