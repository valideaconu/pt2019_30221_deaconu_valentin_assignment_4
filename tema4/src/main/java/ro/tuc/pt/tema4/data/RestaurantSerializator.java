/**
 * 
 */
package ro.tuc.pt.tema4.data;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

import ro.tuc.pt.tema4.business.MenuItem;
import ro.tuc.pt.tema4.business.Order;
import ro.tuc.pt.tema4.business.Restaurant;

/**
 * @author Vali
 *
 */
public class RestaurantSerializator {
	private static String[] fileNames = new String[] {
		"data/Orders.ser",
		"data/Products.ser"
	};
	
	public static void saveProgress(Restaurant restaurant) {
		// Serialization
		try {
			FileOutputStream file = new FileOutputStream(fileNames[0]);
			ObjectOutputStream out = new ObjectOutputStream(file);
			
			out.writeObject(restaurant.getOrderList());
			
			out.close();
			file.close();
			
			System.out.println("Object serialized.");
		} catch (IOException e) { 
			System.out.println("Object unserialized: " + e.getMessage() + ".");
		}
		
		try {
			FileOutputStream file = new FileOutputStream(fileNames[1]);
			ObjectOutputStream out = new ObjectOutputStream(file);
			
			out.writeObject(restaurant.getMenuList());
			out.close();
			file.close();
			System.out.println("Object serialized.");
		} catch (IOException e) { 
			System.out.println("Object unserialized: " + e.getMessage() + ".");
		}
	}
	
	@SuppressWarnings("unchecked")
	public static Restaurant uploadProgress(String restaurantName) {
		ArrayList<Order> orders = null;
		try {
            FileInputStream file = new FileInputStream(fileNames[0]); 
            ObjectInputStream in = new ObjectInputStream(file); 
            
            orders = ((ArrayList<Order>)in.readObject());
            
            in.close(); 
            file.close(); 
        } catch(IOException ex) { 
			System.out.println("Object undeserialized: " + ex.getMessage() + ".");
        	return null;
        } 
        catch(ClassNotFoundException ex) { 
			System.out.println("Object undeserialized: " + ex.getMessage() + ".");
        	return null; 
        } 
		
		ArrayList<MenuItem> menu = null;
		try {
            FileInputStream file = new FileInputStream(fileNames[1]); 
            ObjectInputStream in = new ObjectInputStream(file); 
            
            menu = ((ArrayList<MenuItem>)in.readObject());
            
            in.close(); 
            file.close(); 
        } catch(IOException ex) { 
			System.out.println("Object undeserialized: " + ex.getMessage() + ".");
        	return null;
        } 
        catch(ClassNotFoundException ex) { 
			System.out.println("Object undeserialized: " + ex.getMessage() + ".");
        	return null; 
        } 
		
		
		
		return new Restaurant(restaurantName, orders, menu);
	}
}
