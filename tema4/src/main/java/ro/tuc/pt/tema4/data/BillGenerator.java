package ro.tuc.pt.tema4.data;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class BillGenerator {
	public static String createNewBill(String content) throws IOException {
		String fileName = new String("bills/Factura_");
		fileName += new SimpleDateFormat("yyyy_MM_dd_HH_mm_ss").format(new Date());
		fileName += ".txt";
		
		BufferedWriter writer = new BufferedWriter(new FileWriter(fileName));
		
		writer.write(content);
		
		writer.close();
		
		return fileName;
	}
}
