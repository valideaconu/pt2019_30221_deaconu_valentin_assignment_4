/**
 * 
 */
package ro.tuc.pt.tema4.business;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * @author Vali
 *
 */
public class CompositeProduct extends ArrayList<MenuItem> implements MenuItem, Serializable {
	private static final long serialVersionUID = 6610402333009255225L;
	
	private String compositeName;
	
	public CompositeProduct(String name) {
		super();
		
		this.compositeName = name;
	}
	
	public String getName() {
		return compositeName;
	}
	
	public void setName(String name) {
		this.compositeName = name;
	}
	
	public double computePrice() {
		double price = 0.0;
		for (MenuItem i : this)
			price += i.computePrice();
		
		return price;
	}
	
	public String toString() {
		String str = new String();
		for (MenuItem i : this) {
			str += i.toString();
		}
		return str;
	}
	
	public String getItemsNames() {
		String str = new String();
		for (MenuItem i : this)
			str += i.getName() + ", ";
		
		return str.substring(0, str.length() - 2);
	}

}
