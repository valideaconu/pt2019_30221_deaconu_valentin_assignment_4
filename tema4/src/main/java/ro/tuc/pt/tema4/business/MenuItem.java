/**
 * 
 */
package ro.tuc.pt.tema4.business;

/**
 * @author Vali
 *
 */
public interface MenuItem {
	public double computePrice();
	public String toString();
	public String getName();
}
