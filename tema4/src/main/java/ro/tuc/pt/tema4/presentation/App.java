package ro.tuc.pt.tema4.presentation;

import ro.tuc.pt.tema4.business.Restaurant;
import ro.tuc.pt.tema4.data.RestaurantSerializator;

public class App {
	private static final String APP_NAME = "Restaurant";

    public static void main(String[] args) {
    	Restaurant rest = RestaurantSerializator.uploadProgress("Dolce Vita");
    	
    	WaiterGUI wg;
    	if (rest == null) {
    		rest = new Restaurant("Dolce Vita");
    		wg = new WaiterGUI(APP_NAME, rest, 1);
    	}

		wg = new WaiterGUI(APP_NAME, rest, 1, true);
    	wg.setVisible(true);
    	
    	AdministratorGUI ag = new AdministratorGUI(APP_NAME, rest);
    	ag.setVisible(true);
    	
    	ChefGUI cg = new ChefGUI(APP_NAME, rest);
    	cg.setVisible(true);
    }
}
