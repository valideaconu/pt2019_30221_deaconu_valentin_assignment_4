/**
 * 
 */
package ro.tuc.pt.tema4.business;

import java.io.Serializable;

/**
 * @author Vali
 *
 */
public class BaseProduct implements MenuItem, Serializable {
	private static final long serialVersionUID = -6094628562915680907L;
	
	private String name;
	private double price;
	
	public BaseProduct(String name, double price) {
		this.setName(name);
		this.setPrice(price);
	}
	
	public BaseProduct(String name) {
		this(name, 0);
	}
	
	public BaseProduct() {
		this("Unknown");
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	@Override
	public String toString() {
		return "Item [name=" + name + ", price=" + price + "]";
	}

	public double computePrice() {
		return getPrice();
	}
}
