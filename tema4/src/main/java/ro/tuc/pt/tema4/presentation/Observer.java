/**
 * 
 */
package ro.tuc.pt.tema4.presentation;

/**
 * @author Vali
 *
 */
public interface Observer {
	void update();
}
