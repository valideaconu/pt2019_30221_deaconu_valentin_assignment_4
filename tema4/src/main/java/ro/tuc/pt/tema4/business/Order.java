package ro.tuc.pt.tema4.business;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * @author Vali
 *
 */
public class Order implements Serializable {
	private static final long serialVersionUID = 1050924473935911249L;
	
	private static int AUTO_INCREMENT = 0;
	private int id;
	private String date;
	private int tableId;
	
	private ArrayList<MenuItem> menu;
	
	public Order(int tableId) {
		this.id = AUTO_INCREMENT++;
		
		this.date = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss").format(new Date());
		
		this.tableId = tableId;
		menu = new ArrayList<MenuItem>();
	}

	public int getId() {
		return id;
	}

	public String getDate() {
		return date;
	}

	public int getTableId() {
		return tableId;
	}

	public void setTableId(int tableId) {
		this.tableId = tableId;
	}

	public ArrayList<MenuItem> getMenuItems() {
		return menu;
	}
	
	public boolean add(MenuItem mi) {
		return menu.add(mi);
	}
	
	public double computePrice() {
		double price = 0.0;
		for (MenuItem mi : menu)
			price += mi.computePrice();
		return price;
	}

	@Override
	public String toString() {
		return "Order [id=" + id + ", date=" + date + ", tableId=" + tableId + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((date == null) ? 0 : date.hashCode());
		result = prime * result + id;
		result = prime * result + tableId;
		return result;
	}	
}
