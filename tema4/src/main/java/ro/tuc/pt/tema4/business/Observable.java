/**
 * 
 */
package ro.tuc.pt.tema4.business;

import java.util.ArrayList;

import ro.tuc.pt.tema4.presentation.Observer;

/**
 * @author Vali
 *
 */
public interface Observable {
	final ArrayList<Observer> observerList = new ArrayList<Observer>();
	
	void registerObserver(Observer obs);
	void unregisterObserver(Observer obs);
	void updateObservers();
}
