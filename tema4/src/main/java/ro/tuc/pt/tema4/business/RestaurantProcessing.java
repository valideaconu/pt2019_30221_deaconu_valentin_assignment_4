package ro.tuc.pt.tema4.business;

public interface RestaurantProcessing {
	void createNewMenuItem(MenuItem item);
	void deleteMenuItem(MenuItem item);
	void editMenuItem(MenuItem old_item, MenuItem new_item);
	
	void createNewOrder(Order o);
	double computePrice(Order o);
	void generateBill(Order o);
}
