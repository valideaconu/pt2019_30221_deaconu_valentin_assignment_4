/**
 * 
 */
package ro.tuc.pt.tema4.presentation;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import ro.tuc.pt.tema4.business.MenuItem;
import ro.tuc.pt.tema4.business.Order;
import ro.tuc.pt.tema4.business.Restaurant;
import ro.tuc.pt.tema4.data.RestaurantSerializator;

/**
 * @author Vali
 *
 */
public class ChefGUI extends JFrame implements Observer {
	private static final long serialVersionUID = -2052832551688884158L;
	
	private Restaurant restaurant;
	
	public ChefGUI(String appName, Restaurant rest) {
		this.restaurant = rest;
		
		this.setTitle(appName + " - Chef window");
		this.setSize(980, 560);
		this.setVisible(true);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setLayout(new GridLayout(0, 3));
		
		restaurant.registerObserver(this);
		
		this.addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
		    	RestaurantSerializator.saveProgress(restaurant);
			}
		});
		
		update();
	}

	public void update() {
		this.getContentPane().removeAll();
		this.setLayout(new GridLayout(0, 3));
		
		if (restaurant.getOrderList() != null) {
			for (Order ord : restaurant.getOrderList()) {
				this.getContentPane().add(orderToJPanel(restaurant, ord));
			}
		} else {
			this.getContentPane().add(new JLabel("No orders yet.", SwingConstants.CENTER));
		}
		
		this.revalidate();
		this.repaint();
	}
	
	private static JPanel orderToJPanel(Restaurant restaurant, Order ord) {
		JPanel panel = new JPanel(new BorderLayout());
		JPanel centerPanel = new JPanel(new GridLayout(0, 1));
		
		centerPanel.add(new JLabel(restaurant.getName(), SwingConstants.CENTER));
		double total = 0.0;
		for (MenuItem mi : ord.getMenuItems()) {
			centerPanel.add(new JLabel(mi.toString(), SwingConstants.CENTER));
			total += mi.computePrice();
		}
		centerPanel.add(new JLabel("Total: " + String.format("%.2f", total), SwingConstants.CENTER));
	
		panel.add(centerPanel, BorderLayout.CENTER);
		
		return panel;
	}
}
