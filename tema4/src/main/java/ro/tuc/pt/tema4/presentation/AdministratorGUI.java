/**
 * 
 */
package ro.tuc.pt.tema4.presentation;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.DefaultTableModel;

import ro.tuc.pt.tema4.business.BaseProduct;
import ro.tuc.pt.tema4.business.CompositeProduct;
import ro.tuc.pt.tema4.business.MenuItem;
import ro.tuc.pt.tema4.business.Order;
import ro.tuc.pt.tema4.business.Restaurant;
import ro.tuc.pt.tema4.business.RestaurantProcessing;
import ro.tuc.pt.tema4.data.RestaurantSerializator;

/**
 * @author Vali
 *
 */
public class AdministratorGUI extends JFrame implements RestaurantProcessing {
	private static final long serialVersionUID = 4757736541893873832L;

	private Restaurant restaurant;

	public AdministratorGUI(String appTitle, Restaurant rest) {
		this.setTitle(appTitle + " - Administrator window");
		this.setSize(980, 560);
		this.setVisible(true);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		this.restaurant = rest;
		
		this.setLayout(new BorderLayout());
		
		this.addWindowListener(new WindowAdapter() {
		    public void windowClosing(WindowEvent e) {
		    	RestaurantSerializator.saveProgress(restaurant);
		    }
		});
		
		final JTable table = new JTable();
		
		final DefaultTableModel model = new DefaultTableModel() {
			private static final long serialVersionUID = 1L;
			
			@Override
		    public boolean isCellEditable(int row, int column) {
				if (column == 0)
					return false;
				
				if (column == 3 && !((String) this.getValueAt(row, 2)).equals("-"))
					return false;
				
				return true;
		    }
		};
		
		model.setColumnIdentifiers(new String[] { "ID", "Name", "Contains", "Price" });
		int count = 0;
		for (MenuItem mi : restaurant.getMenuList()) {
			Object[] row = new Object[4];
			row[0] = count++;
			if (mi instanceof BaseProduct) {
				BaseProduct p = (BaseProduct) mi;
				row[1] = p.getName();
				row[2] = "-";
				row[3] = p.computePrice();
			} else if (mi instanceof CompositeProduct) {
				CompositeProduct p = (CompositeProduct) mi;
				row[1] = p.getName();
				row[2] = p.getItemsNames();
				row[3] = p.computePrice();
			}
			model.addRow(row);
		}
		
		model.addTableModelListener(new TableModelListener() {
			public void tableChanged(TableModelEvent e) {
				if (e.getType() == TableModelEvent.UPDATE) {
					int rowChanged = e.getLastRow();
					String contains = (String) table.getValueAt(rowChanged, 2);
					if (contains.equals("-")) {
						double price = 0.0;
						Object obj = table.getValueAt(rowChanged, 3);
						if (obj instanceof String)
							price = Double.valueOf((String) obj);
						else
							price = (Double) obj;
	
						BaseProduct p = new BaseProduct(
								(String) table.getValueAt(rowChanged, 1), 
								price);
						restaurant.set(rowChanged, p);
					} else {
						if (e.getColumn() == 3)
							return;
						
						CompositeProduct p = new CompositeProduct((String) table.getValueAt(rowChanged, 1));
						for (String item : contains.split(", ")) {
							MenuItem m = restaurant.menuListContains(item);
							if (m != null) {
								p.add(m);
							}
						}
						model.setValueAt(p.computePrice(), rowChanged, 3);
						restaurant.set(rowChanged, p);
					}
				}
			}
		});
		
		table.setModel(model);
		
		table.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent e) {
				if (e.getClickCount() == 2) {
					int rowSelected = table.getSelectedRow();
					int ans = JOptionPane.showConfirmDialog(null, "Are you sure you want to delete this item?");
					if (ans == JOptionPane.YES_OPTION) {
						String name = (String) table.getValueAt(rowSelected, 1);
						model.removeRow(rowSelected);
						
						MenuItem m = restaurant.menuListContains(name);
						
						if (m != null) {
							restaurant.getMenuList().remove(m);
						}
					}
				}
			}
		});
		
		JScrollPane sp = new JScrollPane(table, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		this.add(sp, BorderLayout.CENTER);
		
		JButton addNewRow = new JButton("Insert new row");
		addNewRow.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				int lastRow = model.getRowCount();
				Object[] row = new Object[4];
				row[0] = lastRow;
				row[1] = "Unknown";
				row[2] = "-";
				row[3] = 0.0;
				model.addRow(row);
				restaurant.add(new BaseProduct("Unknown", 0.0));
			}
		});
		this.add(addNewRow, BorderLayout.PAGE_END);
	}
	
	public void createNewMenuItem(MenuItem item) {
		
	}

	public void deleteMenuItem(MenuItem item) {
		
	}

	public void editMenuItem(MenuItem old_item, MenuItem new_item) {
			
	}

	public void createNewOrder(Order o) { }

	public double computePrice(Order o) { return 0; }

	public void generateBill(Order o) {	}

}
