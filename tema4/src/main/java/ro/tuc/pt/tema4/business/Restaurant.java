/**
 * 
 */
package ro.tuc.pt.tema4.business;

import java.util.ArrayList;

import ro.tuc.pt.tema4.presentation.Observer;

/**
 * @author Vali
 *
 */
public class Restaurant implements Observable {
	private String name;
	
	private ArrayList<Order> orderList;
	private ArrayList<MenuItem> menuList;
	
	public Restaurant(String name) {
		this.name = name;
		orderList = new ArrayList<Order>();
		menuList = new ArrayList<MenuItem>();
	}
	
	public Restaurant(String name, ArrayList<Order> orderList, ArrayList<MenuItem> menuList) {
		this.name = name;
		this.orderList = orderList;
		this.menuList = menuList;
	}
	
	public boolean add(Order o) {		
		return orderList.add(o);
	}
	
	public boolean remove(Order o) {
		return orderList.remove(o);
	}
	
	public boolean set(int index, Order o) {
		return (orderList.set(index, o) != null);
	}
	
	public boolean add(MenuItem mi) {
		return menuList.add(mi);
	}
	
	public boolean remove(MenuItem mi) {
		return menuList.remove(mi);
	}
	
	public boolean set(int index, MenuItem mi) {
		assert (mi.computePrice() >= 0.0);
		assert (mi.getName().matches("^(?!\\d+$)(?:[a-zA-Z0-9][a-zA-Z0-9 ,]*)?$"));
		
		return (menuList.set(index, mi) != null);
	}
	
	public MenuItem menuListContains(String name) {
		for (MenuItem mi : menuList)
			if (mi instanceof BaseProduct && ((BaseProduct) mi).getName().equals(name))
				return mi;
			else if (mi instanceof CompositeProduct && ((CompositeProduct) mi).getName().equals(name))
				return mi;
		
		return null;
	}
	
	public void registerObserver(Observer obs) {
		observerList.add(obs);
	}

	public void unregisterObserver(Observer obs) {
		observerList.remove(obs);
	}

	public void updateObservers() {
		for (Observer obs : observerList) {
			obs.update();
		}
	}

	public String getName() {
		return name;
	}

	public ArrayList<Order> getOrderList() {
		return orderList;
	}

	public ArrayList<MenuItem> getMenuList() {
		return menuList;
	}

}
